export default ({ env }) => ({
  upload: {
    config: {
      sizeLimit: 1 * 1024 * 1024, // 1mb in bytes
      provider: "@strapi-community/strapi-provider-upload-google-cloud-storage",
      providerOptions: {
        bucketName: env("GCS_BUCKET_NAME"),
        publicFiles: env.bool("GCS_PUBLIC_FILES"),
        uniform: env.bool("GCS_UNIFORM"),
        serviceAccount: env.json("GCS_SERVICE_ACCOUNT"),
        baseUrl: env("GCS_BASE_URL"),
        basePath: env("GCS_BASE_PATH"),
      },
    },
  },
  graphql: {
    config: {
      endpoint: "/graphql",
      shadowCRUD: true,
      playgroundAlways: true,
      depthLimit: 7,
      amountLimit: 100,
      apolloServer: {
        tracing: false,
      },
    },
  },
});
