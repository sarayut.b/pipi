/**
 * A set of functions called "actions" for `boku`
 */

export default {
  bokuRelate: async (ctx, next) => {
    try {
      console.log(ctx.request.query);
      
      ctx.body = {
        data: await strapi.service("api::boku.boku").getBoku(),
      };
    } catch (err) {
      ctx.body = err;
    }
  },
};
