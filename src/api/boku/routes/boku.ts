export default {
  routes: [
    {
     method: 'GET',
     path: '/boku',
     handler: 'boku.bokuRelate',
     config: {
       policies: [],
       middlewares: [],
     },
    },
  ],
};